// Copied off from https://github.com/googlecodelabs/your-first-pwapp/

// Copyright 2016 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// See  https://jakearchibald.com/2014/offline-cookbook/#cache-then-network for caching strategies

var dataCacheName = "cuantotardamiautobus-v1";
var cacheName = "cuantotardamiautobus-v1";
var filesToCache = [
	"./",
	"./index.html",
	"./main.js",
	"./main.css",
	"./lib/leaflet.js",
	"./lib/leaflet.css",
	"./img/icono-normal.png",
	"./img/icono-aviso.png",
	"./lib/jquery-3.2.1.slim.min.js",
	"./lib/leaflet-tilelayer-wmts-src.js",
	"./lib/leaflet.markercluster.js",
	"./lib/MarkerCluster.css"
];

self.addEventListener("install", function(ev) {
	console.log("[ServiceWorker] Install");
	ev.waitUntil(
		caches.open(cacheName).then(function(cache) {
			console.log("[ServiceWorker] Caching app shell");
			return cache.addAll(filesToCache);
		})
	);
});

self.addEventListener("activate", function(ev) {
	console.log("[ServiceWorker] Activate");
	ev.waitUntil(
		caches.keys().then(function(keyList) {
			return Promise.all(
				keyList.map(function(key) {
					if (key !== cacheName && key !== dataCacheName) {
						console.log("[ServiceWorker] Removing old cache", key);
						return caches.delete(key);
					}
				})
			);
		})
	);
	return self.clients.claim();
});

self.addEventListener("fetch", function(ev) {
	if (
		ev.request.method !== "GET" ||
		ev.request.url.indexOf("GetArriveStop") > -1 ||
		ev.request.url.indexOf("piwik") > -1
	) {
		// 		console.log('[Service Worker] Fetch uncached ', ev.request.url);
		/*
		 * When the request URL contains GetArriveStop, the app is asking for fresh
		 * arrival times data. In this case, the service worker always goes to the
		 * network, period.
		 */
		ev.respondWith(fetch(ev.request));
	} else {
		/*
		 * The app is asking for app shell files. In this scenario the app uses the
		 * "Cache, falling back to the network" offline strategy:
		 * https://jakearchibald.com/2014/offline-cookbook/#cache-falling-back-to-network
		 */
		ev.respondWith(
			caches.match(ev.request).then(function(response) {
				if (response) {
					// 					console.log('[Service Worker] Fetch cached ', ev.request.url);
					return response;
				} else {
					return caches
						.open(cacheName)
						.then(function(cache) {
							// 						console.log('[Service Worker] Fetch caching ', ev.request.url);
							return cache.add(ev.request).then(function() {
								return fetch(ev.request);
							});
						})
						.catch(function(err) {
							// 						console.log('[Service Worker] Could not cache ', ev.request.url, err);
							return fetch(ev.request);
						});
				}

				// 				return response || fetch(ev.request);
			})
		);
	}
});
