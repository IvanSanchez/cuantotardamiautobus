// Redireccionar a HTTPS si es necesario
if (
	window.location.protocol === "http:" &&
	(window.location.host === "www.cuantotardamiautobus.es" ||
		window.location.host === "cuantotardamiautobus.es")
) {
	_paq.push(["trackEvent", "flow", "redirect-https"]);
	window.location =
		"https://www.cuantotardamiautobus.es/madrid/" + window.location.hash;
}

// Enable service worker to agressively cache stuff
if ("serviceWorker" in navigator) {
	_paq.push(["trackEvent", "serviceworker", "available"]);

	navigator.serviceWorker.register("./service-worker.js").then(function() {
		console.log("Service Worker Registered");
		_paq.push(["trackEvent", "serviceworker", "installed"]);
	});
}

// Map stuff
var madridBounds = L.latLngBounds([[40.23, -4.0], [40.64, -3.49]]);
var map = L.map("map", { maxBounds: madridBounds }).fitBounds(madridBounds);

var ign = new L.TileLayer.WMTS("https://www.ign.es/wmts/ign-base", {
	layer: "IGNBaseTodo-nofondo",
	tilematrixSet: "GoogleMapsCompatible",
	format: "image/png",
	attribution:
		"<a href='http://www.cartociudad.es/portal/'>Cartociudad</a> CC-BY-SA  <a href='http://www.ign.es'>IGN</a>, <a href='http://www.scne.es/'>Sistema Cartográfico Nacional</a>",
	maxZoom: 20,
	crossOrigin: true
}).addTo(map);

// var layer = L.tileLayer('http://{s}.tile2.opencyclemap.org/transport/{z}/{x}/{y}.png', {
var thunderforest_transport = L.tileLayer(
	"https://{s}.tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=0666e067239e488e9ae00a0a9f21edbd",
	{
		attribution:
			'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, Imagery courtesy <a href="http://www.thunderforest.com/">Andy Allan</a>',
		maxNativeZoom: 18,
		maxZoom: 20,
		prefix: false,
		crossOrigin: true
	}
);

L.control
	.layers({
		"IGN Cartociudad": ign,
		"OSM transporte": thunderforest_transport
	})
	.addTo(map);

	
// Ñapear el proveedor de cartociudad para filtrar resultados por municipio de madrid
var proveedorCartociudad = new leafletGeosearch.CartociudadProvider();
proveedorCartociudad.parse = function(data) {
	return leafletGeosearch.CartociudadProvider.prototype.parse.apply(this, 
		data.filter(function(result){
			return result.muni == "Madrid";
		})
	);
}
// Ñapear el proveedor de cartociudad para que pida todo con ", Madrid" al final
proveedorCartociudad.getParamString = function(params) {
	params.q += ", Madrid";
	params.no_process = "municipio,poblacion";
	return leafletGeosearch.CartociudadProvider.prototype.getParamString.call(this, params);
}


	
var geosearchControl = new leafletGeosearch.GeoSearchControl({
	provider: proveedorCartociudad,
	autoclose: true
})

geosearchControl.addTo(map);

map.on("geosearch/showlocation ", function onLocationFound(ev) {
	_paq.push(["trackEvent", "map", "geocoder", ev.location]);
});

map.on("baselayerchange ", function onLocationFound(ev) {
	_paq.push(["trackEvent", "map", "baselayerchange", ev.name]);
});

map.on("locationfound", function onLocationFound(e) {
	if (madridBounds.contains(e.latlng)) {
		map.flyToBounds(e.bounds);
		_paq.push(["trackEvent", "map", "geolocate-in"]);
	} else {
		_paq.push(["trackEvent", "map", "geolocate-out"]);
	}

	// 	var radius = e.accuracy / 2;
	// 		L.marker(e.latlng).addTo(map)
	// 			.bindPopup("You are within " + radius + " meters from this point").openPopup();
	// 	map.setView(e.latlng,17);
	// 		L.circle(e.latlng, radius, {fillOpacity: 0.3 } ).addTo(map);
});

if (!window.location.hash.slice(1)) map.locate({ setView: false, maxZoom: 18 }); // Sólo activar la geolocalización cuando no me pasen un permalink.

// map.addControl(new L.Control.Permalink({text: 'Permalink', layers: [layer]}));
map.attributionControl.setPrefix(false);

// A wrapper to fetch stuff from the EMT API.
// Returns a Promise to a set of data, based on the Fetch API. This means
// that rejections will happen the same as Fetch.
function fetchEmt(endpoint, params) {
	params = params || {};

	var server = "https://openbus.emtmadrid.es:9443/emt-proxy-server/last/";

	var idClient = "WEB.SERV.dumbstumpy@mailinator.com";
	var passKey = "C513925F-1DBE-441C-B6C8-716E55E0C0A1";

	var formdata = new FormData();
	formdata.append("idClient", idClient);
	formdata.append("passKey", passKey);

	for (var i in params) {
		formdata.append(i, params[i]);
	}

	return fetch(server + endpoint, {
		method: "POST",
		body: formdata
	})
		.then(function(res) {
			return res.json();
		})
		.then(function(data) {
			if ("resultCode" in data) {
				// "resultCode" viene del puente SOAP-JSON. Sólo lo he visto
				// cuando la petición es correcta.
				if (data.resultCode === 0) {
					return data.resultValues;
				} else {
					return Promise.reject(data.resultCode + " " + data.resultDescription);
				}
			} else if ("ReturnCode" in data) {
				// "ReturnCode" viene del servicio SOAP, al parecer el puente hace
				// passthrough de los datos directamente en caso de error.
				if (data.ReturnCode === 0) {
					return data.Result || data.RESULTADO || data.resultValues;
				} else {
					return Promise.reject(data.ReturnCode + " " + data.Description);
				}
			}

			return data;
		});
}

// var stopsGroup = L.featureGroup().addTo(map);
var stopsGroup = L.markerClusterGroup({
	disableClusteringAtZoom: 20,
	maxZoom: 20,
	spiderfyOnMaxZoom: false,
	showCoverageOnHover: false,
	maxClusterRadius: 64,
	iconCreateFunction: function(cluster) {
		return L.divIcon({
			html: "<div><span></span></div>",
			className: "marker-cluster",
			iconSize: new L.Point(40, 40)
		});
	}
}).addTo(map);
// var iconNoWarn = L.icon({iconUrl: 'img/icono-normal.png', shadowUrl: false, iconSize: [16, 16], iconAnchor: [8, 8]})
var iconNoWarn = L.divIcon({
	html: "<div><span></span></div>",
	className: "one-marker",
	iconSize: new L.Point(26, 26)
});
// var iconWarn = L.icon({iconUrl: 'img/icono-aviso.png', shadowUrl: false, iconSize: [16, 16], iconAnchor: [8, 8]})
// var iconWarn = L.icon({});

var stopsData = [];
var stopMarkers = [];

fetchEmt("/bus/GetNodesLines.php", {})
	.then(function(data) {
		_paq.push(["trackEvent", "flow", "stops-list-requested"]);
		stopsData = data;
		stopPoints = [];

		for (var i = 0, l = data.length; i < l; i++) {
			var marker = L.marker([data[i].latitude, data[i].longitude], {
				icon: iconNoWarn
			});
			marker.on("click", onStopClick(data[i]));
			// 		stopsGroup.addLayer(marker);
			stopMarkers.push(marker);
		}

		stopsGroup.addLayers(stopMarkers);

		var stopHash = window.location.hash.indexOf("parada=");
		if (stopHash !== -1) {
			var stopId = Number(window.location.hash.substr(stopHash + 7));
			for (var j = 0, l = stopsData.length; j < l; j++) {
				if (stopsData[j].node === stopId) {
					_paq.push(["trackEvent", "flow", "initial-stop", stopId]);
					onStopClick(stopsData[j])({ target: stopMarkers[j] });

					map.setView([stopsData[j].latitude, stopsData[j].longitude], 18);

					// Espeeera un momentito. Si centramos la parada, no se va a ver bien en dispositivos móviles. ¿Qué tal si la dejamos a 3/4 de altura, para que se vea más popup?
					L.Util.requestAnimFrame(function() {
						var y = window.innerHeight * 0.25;
						var x = window.innerWidth * 0.5;
						map.setView(
							map.containerPointToLatLng(new L.Point(x, y, true)),
							18
						);
					});
					return;
				}
			}
		}
	})
	.catch(function(err) {
		_paq.push(["trackEvent", "flow", "stop-lists-error", err]);
		$("#overlay").empty();
		$("#overlay").html(
			"<div><span onclick=\"$('#overlay').hide(); $('#overlay-background').hide(); $('.leaflet-control').show(); currentStopId = undefined;\" class='clickable backbutton'>Volver al mapa</span></div>" +
				"<p>Parece que no tienes conexión a Internet, y lamentablemente cuantotardamiautobús no puede funcionar. 😢 </p>"
		);
		$("#overlay-background").show();
		$("#overlay").show();
		$(".leaflet-control").hide();
	});

$("#zoomtip").hide();

var currentStopId;
var currentStopName;
var currentPopup;
var currentRequest;

// Event handler for the marke click
function onStopClick(stopData) {
	return function(ev) {
		map.closePopup();
		currentPopup = L.popup().setLatLng(ev.target.getLatLng());

		_paq.push(["trackEvent", "wait-time", "request", stopData.node]);
		currentPopup.setContent(
			"<p>Solicitando tiempos de llegada para " +
				stopData.name +
				" - " +
				stopData.node +
				"</p>"
		);
		currentPopup.openOn(map);

		// 		map.openPopup(stopData.name + ' - ' + stopData.node, ev.target.getLatLng());
		// 		ev.target.bindPopup().openPopup();

		currentStopId = stopData.node;
		currentStopName = stopData.name;
		queryAndDisplayArrivals(currentStopId);
	};
}

function queryAndDisplayArrivals(stopId) {
	fetchEmt("/geo/GetArriveStop.php", { idStop: stopId })
		.then(function(data) {
			// 		console.log(data);
			// Prevent displaying arrivals in case of race conditions
			if (currentStopId !== stopId) {
				return;
			}

			var text;

			if (data[0] === false || !data.arrives.length) {
				_paq.push(["trackEvent", "wait-time", "empty", stopId]);
				text =
					"Actualmente no pasan autobuses por " +
					currentStopName +
					" (" +
					currentStopId +
					")";
			} else {
				text =
					"Autobuses que pasan por<br/>" +
					currentStopName +
					" (" +
					currentStopId +
					"):<table class='timetable'><tr><th>Línea</th><th>Destino</th><th>Tiempo</th></tr>";
				var arrivals = data.arrives;
				_paq.push(["trackEvent", "wait-time", "ok", stopId, arrivals.length]);

				for (var i = 0, l = arrivals.length; i < l; i++) {
					var arrival = arrivals[i];

					// Prevent displaying arrivals in case of race conditions
					if (currentStopId !== arrival.stopId) {
						return;
					}

					var minutes = Math.floor(arrival.busTimeLeft / 60);
					var seconds = arrival.busTimeLeft % 60;
					var countdown_class = "countdown"; // Se asigna una clase CSS a las celdas que contienen un tiempo, para meterle una cuenta atrás con un SetInterval.
					if (seconds < 10) seconds = "0" + seconds;

					var time = minutes + ":" + seconds;
					if (arrival.busTimeLeft == 999999) {
						time = "20+";
						countdown_class = "";
					} else if (arrival.busTimeLeft <= 0) {
						countdown_class = "";
					}

					// 					text += "<tr><td class='numero'><span class='clickable' onclick='mostrar_mapa_linea(\"" + data[i].linea + "\");'>" + data[i].linea +
					text +=
						"<tr><td class='numero'><span>" +
						arrival.lineId +
						"</td><td class='destino'>" +
						arrival.destination +
						"</td><td class='numero " +
						countdown_class +
						"'>" +
						time +
						"</td></tr>";
				}
				text += "</table>";
			}

			if (window.outerWidth > 600) {
				// 					text += "<div class='clickable' onclick='window.location = \"http://www.cuantotardamiautobus.es/createqr.php?ciudad=madrid&parada="
				// 					+ currentStopId + "\";'>Crear código QR</div>";

				// 				$('.leaflet-popup-content').html(text);
				currentPopup.setContent(text);
			} else {
				$("#overlay").empty();
				$("#overlay").html(
					"<div><span onclick=\"$('#overlay').hide(); $('#overlay-background').hide(); $('.leaflet-control').show(); currentStopId = undefined;\" class='clickable backbutton'>Volver al mapa</span></div>" +
						text
				);
				$("#overlay-background").show();
				$("#overlay").show();
				$(".leaflet-control").hide();

				// Se cierra el popup del mapa, y se vuelve a poner el hash (que se ha quitado al cerrar el popup)
				tempId = currentStopId;
				map.closePopup();
				currentStopId = tempId;
			}
			window.location.hash = "parada=" + currentStopId;

			// ...and repeat.
			if (currentRequest) {
				clearTimeout(currentRequest);
			}
			currentRequest = setTimeout(
				queryAndDisplayArrivals.bind(this, stopId),
				30000
			);
		})
		.catch(function(err) {
			_paq.push(["trackEvent", "wait-time", "error", stopId]);
			currentPopup.setContent(
				"<p>Error al solicitar tiempos de parada. ¿Quizás no tienes conexión a Internet?<p><p><small>" +
					err.toString() +
					"</small></p>"
			);
		});
}

map.on("popupclose", function() {
	_paq.push(["trackEvent", "wait-time", "close"]);
	window.location.hash = "";
	clearTimeout(currentRequest);
	currentStopId = null;
});

setInterval(function(data) {
	$(".countdown").each(function(i) {
		var time = this.innerHTML;

		separatorPosition = time.search(":");
		var minutes = parseInt(time.slice(0, separatorPosition));
		var seconds = parseInt(time.slice(separatorPosition + 1));

		seconds += minutes * 60;

		seconds -= 1;

		if (seconds <= 0) {
			// 				this.className = "numero";	// Quitar la clase "countdown" == la cuenta atrás no se ejecutará para esta celda
			$(this).removeClass("countdown"); // Quitar la clase "countdown" == la cuenta atrás no se ejecutará para esta celda
			this.innerHTML = "0:00";
		} else if (minutes >= 20 || isNaN(seconds) || isNaN(minutes)) {
			// Vamos a evitar que la gente vea un NaN:NaN...
			// 				this.className = "numero";	// Quitar la clase "countdown" == la cuenta atrás no se ejecutará para esta celda
			$(this).removeClass("countdown"); // Quitar la clase "countdown" == la cuenta atrás no se ejecutará para esta celda
		} else {
			minutes = Math.floor(seconds / 60);
			seconds = seconds % 60;
			if (seconds < 10) {
				seconds = "0" + seconds;
			}

			this.innerHTML = minutes + ":" + seconds;
		}
	});
}, 1075); // This is very, very evil.
