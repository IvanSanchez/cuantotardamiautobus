# cuantotardamiautobus

El código detrás de www.cuantotardamiautobus.es.

Una web que muestra los tiempos de llegada de los autobuses de la EMT de Madrid
en un formato más amigable (para mi gusto) que su web.

# Licencia

Este código está bajo licencia GPL. Puede encontrarse el texto completo en el fichero COPYING.txt.

Se incluyen iconos de Wikimedia Commons, bajo dominio público. Véase https://en.wikipedia.org/wiki/Wikipedia:Route_diagram_template/Catalog_of_pictograms



# Instalación

Después de cuatro años con código en PHP sin mantener, que había que instalar de mala manera, ahora el código prescinde totalmente de la parte servidor, y cosas más modernas de JS, como fetch y promesas, para pedir los datos directamente a la API de EMT.

Así que esto no necesita instalación. Uno puede descargar una copia del código, visitar la página en local, y todo debería de funcionar. Si no, ejecutar un [`light-server`](https://github.com/txchen/light-server) o cualquier otro servidor web usando los ficheros en `src/` soluciona el tema de service workers y orígenes de datos y todas esas historias.

Las claves de API de EMT están cableadas en el código, en `main.js`.

